#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int takestone(int *, int, int, int);

int main()
{
  // nStone:石の数 nGet:取れる石の数 nSente:1なら人が先手
  // nOrder:1なら先手、-1なら後手の番

  int nStone, nGet, nSente, nOrder, nResult;
  char answer[8];

  printf("石を交互に取り、最後の１個をとった人が負けです¥n");
  while (1)
  {
    nOrder = 1;
    printf("石の数は(５以上100以下)==");
    gets(answer);
    nStone = atoi(answer);
    if (nStone < 5 || nStone > 100)
    {
      printf("石の数が不正です¥n");
      continue;
    }
    while (1)
    {
      printf("一度に取れる石の数は(2以上)==");
      gets(answer);
      nGet = atoi(answer);
      if (nGet >= nStone)
      {
        printf("一度に取れる石の数が多すぎます¥n");
        continue;
      }
      if (nGet < 2)
      {
        printf("一度に取れる石の数が少なすぎます¥n");
        continue;
      }
      break;
    }
    printf("あなたが先手になりますか(Y/N)--");
    gets(answer);
    if (strcmp(answer, "y") == 0 || strcmp(answer, "Y") == 0)
    {
      nSente = 1;
    }
    else
    {
      nSente = 0;
    }
    while (1)
    {
      nResult = takestone(&nStone, nGet, nSente, nOrder);
      if (nResult == -1)
        break;
      nOrder *= -1;
    }
    printf("続けますか(Y/N)--");
    gets(answer);
    if (strcmp(answer, "n") == 0 || strcmp(answer, "N") == 0)
      break;
    printf("================================¥n¥n");
  }
  return 0;
}

int takestone(int *pstone, int nGet, int nSente, int nOrder)
{
  char answer[8];
  int n;

  srand((unsigned)time(NULL));

  if ((nOrder == 1 && nSente == 1) || (nOrder == -1 && nSente == 0))
  {
    while (1)
    {
      printf("取る石の数は--");
      gets(answer);
      n = atoi(answer);
      if (n > nGet)
      {
        printf("一度に取れる石の数は%d個までです¥n", nGet);
        continue;
      }
      if (n <= 0)
      {
        printf("1個以上を指定してください¥n");
        continue;
      }
      if (n >= *pstone)
      {
        printf("そのような取り方は出来ません¥n");
        continue;
      }
      *pstone -= n;
      if (*pstone == 1)
      {
        printf("あなたの勝ちです¥n");
        return -1;
      }
      break;
    }
  }
  else
  {
    if (*pstone <= nGet + 1)
    {
      n = *pstone - 1;
    }
    else
    {
      n = rand() % nGet + 1;
    }
    printf("コンピュータは%d個取りました¥n", n);
    *pstone -= n;
    if (*pstone == 1)
    {
      printf("コンピュータの勝ちです¥n");
      return -1;
    }
  }
  printf("残りの石は%d個です¥n", *pstone);
  return 0;
}