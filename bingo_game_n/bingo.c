#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 256 // 1行の最大文字数(バイト数)

// Prototype define
void readFileFormat(FILE *fp, int *row, int *col);
void shuffle(int array[], int size);
// 配列渡すときに要素数を保持できないので、関数の引数に追加する
// https://www.sejuku.net/blog/24793
void create_rand_num(int **bingo_array, int *input_array, int bingo_size);
bool check_bingo(int **bingo, int bingo_size);

int main(int argc, char *argv[])
{
  int i, j;
  int n = 10;
  int rand_array_num = 0;
  int **bingo;
  int *rand_array;
  int row, col;
  int column_counts = 0;
  bool initial_read = true;
  bool gameover_flg = false;
  FILE *fp;
  char str[N];
  char *ptr;

  if (argc != 2)
  {
    printf("Inputを指定してください");
    return -1;
  }

  // inputを読み込む
  fp = fopen(argv[1], "r");
  if (fp == NULL)
  {
    printf("ファイル名が不当です。^r");
    return -1;
  }

  readFileFormat(fp, &row, &col);
  n = col;
  fclose(fp);

  //bingoの中身を見るために読み込みし直す
  fp = fopen(argv[1], "r");
  // init of bingo
  bingo = malloc(sizeof(int *) * n);
  for (i = 0; i < n; i++)
  {
    bingo[i] = malloc(sizeof(int) * n);
  }

  // iとjを初期化する
  i = 0;
  while (fgets(str, N, fp) != NULL)
  {
    // iとjを初期化する
    j = 0;
    //  カンマを区切りにptr文字列を分割
    // 1回目
    column_counts = 1;
    ptr = strtok(str, "\t");
    bingo[i][j] = atoi(ptr);
    // 2回目以降
    while (ptr != NULL)
    {
      j++;
      // strtok関数により変更されたNULのポインタが先頭
      ptr = strtok(NULL, "\t");
      // ptrがNULLの場合エラーが発生するので対処
      if (ptr != NULL)
      {
        // printf("%s", ptr);
        bingo[i][j] = atoi(ptr);
      }
    }
    i++;
  }
  fclose(fp);

  // 何分割かチェック
  // 2次元配列を作成して取り込む
  for (i = 0; i < n; i++)
  {
    for (j = 0; j < n; j++)
    {
      printf("%2d ", bingo[i][j]);
    }
    printf("\n");
  }
  // 乱数格納用の配列を作成する
  rand_array = malloc(sizeof(int *) * n * n);
  create_rand_num(bingo, rand_array, n);
  // キーの入力待ち
  while (gameover_flg != true)
  {
    printf("キー入力をしてください。 (qで終了。それ以外は抽選開始)\n");
    // Enterなら抽選開始、qなら終了
    char player_input[2];
    do
    {
      fgets(player_input, 2, stdin);
      if (!strcmp(player_input, "q"))
      {
        gameover_flg = true;
        printf("ゲームを終了します。\n");
        // free
        free(rand_array);
        for (i = 0; i < n; i++)
        {
          free(bingo[i]);
        }
        free(bingo);
        return 0;
      }
    } while ((player_input[0] != '\n') && (player_input[1] != '\n'));
    // scanf("%s", player_input);

    printf("抽選開始\n");
    printf("%d\n", rand_array[rand_array_num]);
    // update bingo
    for (int i = 0; i < n; i++)
    {
      for (int j = 0; j < n; j++)
      {
        /* code */
        if (bingo[i][j] == rand_array[rand_array_num])
        {
          bingo[i][j] = 0;
        }
      }
    }
    for (int i = 0; i < n; i++)
    {
      for (int j = 0; j < n; j++)
      {
        printf("%2d ", bingo[i][j]);
      }
      printf("\n");
    }
    rand_array_num++;
    // check bingo
    if (check_bingo(bingo, n))
    {
      printf("ビンゴしました。\n");
      gameover_flg = true;
    }
  }

  // free
  free(rand_array);
  for (i = 0; i < n; i++)
  {
    free(bingo[i]);
  }
  free(bingo);
  return 0;
}
void shuffle(int *array, int size)
{
  int i = size;
  while (i > 1)
  {
    int j = rand() % i;
    i--;
    int t = array[i];
    array[i] = array[j];
    array[j] = t;
  }
}
// ビンゴ用の抽選結果を配列に保存する
void create_rand_num(int **bingo_array, int *input_array, int bingo_size)
{
  // create rand num
  int size = bingo_size;
  int bingo_num;
  srand((unsigned)time(NULL));

  for (int i = 0; i < size * size; i++)
  {
    input_array[i] = i + 1;
  }
  shuffle(input_array, size * size);
  for (int i = 0; i < size * size; i++)
  {
    printf("%d\n", input_array[i]);
  }
  return;
}

// If bingo is complete, return true
bool check_bingo(int **bingo_array, int bingo_size)
{
  bool b_check_result = false;
  // vertical
  for (int j = 0; j < bingo_size; j++)
  {
    bool b_check_vertical = true;
    for (int i = 0; i < bingo_size; i++)
    {
      if (bingo_array[i][j] != 0)
      {
        b_check_vertical = false;
      }
    }
    if (b_check_vertical)
    {
      printf("bingo vertical");
      b_check_result = true;
      return b_check_result;
    }
  }

  // horizontally
  for (int i = 0; i < bingo_size; i++)
  {
    bool b_check_horizon = true;
    for (int j = 0; j < bingo_size; j++)
    {
      if (bingo_array[i][j] != 0)
      {
        b_check_horizon = false;
      }
    }
    if (b_check_horizon)
    {
      printf("bingo horizon");
      b_check_result = true;
      return b_check_result;
    }
  }
  // cross
  bool b_check_cross_left = true;
  for (int i = 0; i < bingo_size; i++)
  {
    if (bingo_array[i][i] != 0)
    {
      b_check_cross_left = false;
    }
  }

  if (b_check_cross_left)
  {
    printf("bingo left");
    b_check_result = true;
  }

  bool b_check_cross_right = true;
  for (int i = 0; i < bingo_size; i++)
  {
    // printf("bingo_array[%d][%d]=%d\n", i, sizeof(bingo_array) / sizeof(int) - i, bingo_array[i][sizeof(bingo_array) / sizeof(int) - i]);
    if (bingo_array[i][bingo_size - i] != 0)
    {
      b_check_cross_right = false;
    }
  }

  if (b_check_cross_right)
  {
    /* code */
    printf("bingo right");
    b_check_result = true;
  }
  return b_check_result;
}

void readFileFormat(FILE *fp, int *row, int *col)
{
  int data;
  char buf[256], *token;

  rewind(fp);

  *row = *col = 0;
  while (fgets(buf, 256, fp))
  {
    if (*row == 0)
    {
      token = (char *)strtok(buf, "\t");
      while (token != NULL)
      { // buf にトークンがなくなるまで繰り返す
        sscanf(token, "%d", &data);
        token = (char *)strtok(NULL, "\t"); // 次のトークンを取得
        *col += 1;
      }
    }
    *row += 1;
  }
}