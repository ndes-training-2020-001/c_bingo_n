# coding: UTF-8
import random #randomのインポート
import numpy as np #numpyのインポート
import cv2  #OpenCVのインポート

img=np.zeros((500, 500, 3), np.uint8) #500x500x3要素の3次元ndarrayを生成し値を0に
img[:,:,0:3]=[51,40,92]  #全画素にBGR=51,40,92(ダークブラウン)を設定
#横線を引く：y100からy400まで100ピクセルおきに白い(BGRすべて255)横線を引く
img[100:500:100, :, :] = 255
#縦線を引く：x100からx400まで100ピクセルおきに白い(BGRすべて255)縦線を引く
img[:, 100:500:100, :] = 255

bingo = list(range(1,71)) #1～70まで70個の連続した数値からなるリスト'bingo'を生成

for i in range(3): #作るビンゴカード枚数分、繰り返す

    bingo_img=np.copy(img) #縦横線を引いた画像ingをbingo_imgにコピー

    #リスト'bingo'からランダムに25個の要素を重複なしに選択、リスト'bingo_25'に代入
    bingo_25=random.sample(bingo, 25)
    bingo_25n=np.array(bingo_25) #リスト'bingo_25'をndarray'bingo_25n'に変換
    bingo_5x_5x=bingo_25n.reshape(5,5) #ndarray'bingo_25n'を5行5列に変換

    text_y=70 #文字表示位置のy座標
    for y in range(5): #yを0～4まで繰り返す
        text_x=10  #文字表示位置のx座標
        for x in range(5): #xを0～4まで繰り返す
            bingo_number=str(bingo_5x_5x[y,x])
            #文字列bingo_numberを座標(text_x, text_y)にサイズ2、白、文字太さ3で描画
            cv2.putText(bingo_img, bingo_number, (text_x, text_y), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), thickness=3)
            text_x=text_x+100 #文字表示位置のx座標を100ピクセルずらす
        text_y=text_y+100 #文字表示位置のy座標を100ピクセルずらす

    save_filename='bingocard'+str(i)+'.png' #保存用ファイル名設定 bingocard0.png,bingocard1.pngのような連番
    cv2.imwrite(save_filename,bingo_img) #bingo_imgを保存
